#!/bin/bash
echo "[2]- Install elasticsearch and kibana version 7.x"
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update
sudo apt-get install -q -y vim curl jq apt-transport-https elasticsearch=7.9.1 kibana=7.9.1 &>/dev/null
#sudo cp /vagrant/conf/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml
sudo cp /vagrant/conf/kibana/kibana.yml /etc/kibana/kibana.yml
sudo systemctl enable elasticsearch.service
sudo systemctl enable kibana.service
sudo systemctl start elasticsearch.service
sudo systemctl start kibana.service

echo "[3]- Install nginx server version 1.19.6"
sudo apt-get install -q -y libpcre3-dev zlib1g-dev >/dev/null
wget http://nginx.org/download/nginx-1.19.6.tar.gz -q >/dev/null
tar xfz nginx-1.19.6.tar.gz
cd nginx-1.19.6/
./configure --with-http_stub_status_module --with-http_realip_module >/dev/null
make >/dev/null
sudo make install >/dev/null
cp /vagrant/conf/nginx/nginx.conf /usr/local/nginx/conf/nginx.conf
cd /usr/local/nginx/sbin/
sudo ./nginx -c /usr/local/nginx/conf/nginx.conf
sudo ./nginx -s reload


echo "[4]- Install filebeat and metricbeat version 7.9.1"
cd ~
curl -L -O -s https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.9.1-amd64.deb
sudo dpkg -i metricbeat-7.9.1-amd64.deb
sudo metricbeat modules enable nginx
sudo cp /vagrant/conf/metricbeat/nginx.yml /etc/metricbeat/modules.d/nginx.yml
sudo cp /vagrant/conf/metricbeat/system.yml /etc/metricbeat/modules.d/system.yml

curl -L -O -s https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.9.1-amd64.deb
sudo dpkg -i filebeat-7.9.1-amd64.deb
sudo filebeat modules enable nginx
sudo cp /vagrant/conf/filebeat/nginx.yml /etc/filebeat/modules.d/nginx.yml

#Wait for kibana to be up
while [[ "$(curl --silent -X GET http://localhost:5601/api/status | jq '.status.overall.state')" != "\"green\"" ]] ; do sleep 5 ; done

sudo filebeat setup --index-management --dashboards -E setup.dashboards.directory=/vagrant/conf/filebeat/
sudo metricbeat setup --index-management --dashboards -E setup.dashboards.directory=/vagrant/conf/metricbeat/
sudo systemctl start filebeat
sudo systemctl start metricbeat

