resource "aws_instance" "logstash" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.small"
  key_name = var.keypair_name
  vpc_security_group_ids = [aws_security_group.logstash_sg.id]
  subnet_id = element(aws_subnet.public_subnets.*.id, 0)
  availability_zone = element(var.availability_zones, 0)
  associate_public_ip_address = true

  provisioner "remote-exec" {
    connection {
      host = self.public_ip
      private_key = file(var.private_key)
      user = var.ansible_user
    }
    inline = ["sudo hostnamectl set-hostname logstash"]
  }
  tags = {
    Name = "terraform-aws-logstash"
  }
}

  resource "null_resource" "init_logstash_instance" {
  depends_on = [aws_instance.logstash, aws_elasticsearch_domain.es]
  provisioner "local-exec" {
    command = <<EOT
      cd ../ansible;
      export ANSIBLE_HOST_KEY_CHECKING=False;
      ansible-playbook -u ${var.ansible_user} \
                       -i ${aws_instance.logstash.public_ip}, playbook-logstash.yml \
                       -e "elasticsearch_host=${aws_elasticsearch_domain.es.endpoint} \
                           region=${var.aws_region} \
                           access_key=${var.aws_access_key_id} \
                           secret_key=${var.aws_secret_access_key}"
    EOT
  }
}

output "logstash_ip" {
  value = aws_instance.logstash.public_ip
}

