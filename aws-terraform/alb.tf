resource "aws_alb_target_group" "alb-target-group" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

  name        = "terraform-aws-alb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.main_vpc.id

  stickiness {
    type = "lb_cookie"
    enabled = true
  }

  tags = {
    Name = "terraform-aws"
  }
}

resource "aws_alb_target_group_attachment" "alb-attachment-web" {
  count = length(aws_instance.web)
  target_group_arn = aws_alb_target_group.alb-target-group.arn
  target_id = element(aws_instance.web.*.id, count.index)
  port = 80
}


resource "aws_alb" "alb" {
  name = "terraform-aws-alb"
  internal = false
  ip_address_type = "ipv4"
  load_balancer_type = "application"
  idle_timeout = 60
  security_groups = [
    aws_security_group.alb_sg.id
  ]
  subnets = aws_subnet.public_subnets.*.id
}

resource "aws_alb_listener" "http_listener" {
  load_balancer_arn = aws_alb.alb.arn
  port = 80
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_alb_target_group.alb-target-group.arn
  }
}



