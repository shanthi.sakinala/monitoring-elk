resource "aws_instance" "web" {
  count = length(var.private_app_subnet_cidrs)
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = var.keypair_name
  vpc_security_group_ids = [aws_security_group.web_sg.id]
  subnet_id = element(aws_subnet.public_subnets.*.id, count.index)
  availability_zone = element(var.availability_zones, count.index)
  associate_public_ip_address = true

  provisioner "remote-exec" {
    connection {
      host = self.public_ip
      private_key = file(var.private_key)
      user = var.ansible_user
    }
    inline = ["sudo hostnamectl set-hostname wordpress-${count.index + 1}"]
  }
  tags = {
    Name = "terraform-aws-public-web-server-${count.index + 1}"
  }
  depends_on = [ aws_db_instance.mysql]
}

resource "null_resource" "init_wordpress_instances" {
  depends_on = [aws_instance.web, aws_db_instance.mysql, aws_instance.logstash, aws_elasticsearch_domain.es]
  //count = length(aws_instance.web)
  provisioner "local-exec" {
    command = <<EOT
      cd ../ansible;
      export ANSIBLE_HOST_KEY_CHECKING=False;
      ansible-playbook -u ${var.ansible_user} \
                       -i ${aws_instance.web.*.public_ip[0]},${aws_instance.web.*.public_ip[1]}, playbook-wordpress.yml \
                       -e "wp_db_host=${aws_db_instance.mysql.address} \
                           vpc_cidr=${aws_vpc.main_vpc.cidr_block}
                           wp_db_name=${var.db_name} \
                           wp_db_user=${var.db_user} \
                           wp_db_password=${var.db_password} \
                           elasticsearch_host=https://${aws_elasticsearch_domain.es.endpoint}:443 \
                           kibana_host=https://${aws_elasticsearch_domain.es.endpoint}:443/_plugin/kibana/ \
                           logstash_host=${aws_instance.logstash.private_ip}:5044 \
                           region=${var.aws_region} \
                           access_key=${var.aws_access_key_id} \
                           secret_key=${var.aws_secret_access_key}"
    EOT
  }
}

locals {
  template_runners = templatefile("${path.module}/templates/config.toml", {
    host = aws_instance.bastion.public_ip,
    user = "ubuntu",
    es-endpoint = aws_elasticsearch_domain.es.endpoint
  })
}

resource "local_file" "ssh_jump_conf" {
  content = local.template_runners
  filename = "auto-generated/config"
}


output "webserver1_ip" {
  value = aws_instance.web.*.public_ip[0]
}

output "webserver2_ip" {
  value = aws_instance.web.*.public_ip[1]
}
