variable "aws_region" {
  description = "The aws region"
}
variable "ssh_key" {
  description = "Default ssh pub key"
  default     = "~/.ssh/id_rsa.pub"
}

variable "vpc_cidr" {
  description = "CIDR for the aws VPC"
  default = "10.0.0.0/16"
}

variable "public_subnet_cidrs" {
  description = "CIDR for the public subnets"
  type = list
}

variable "private_app_subnet_cidrs" {
  description = "CIDR for the private app subnets"
  type = list
}

variable "private_data_subnet_cidrs" {
  description = "CIDR for the private data subnets"
  type = list
}


variable "availability_zones" {
  type = list
}

variable db_name {
  description = "db name"
}
variable ansible_user {
  description = "ansible user"
  default = "ubuntu"
}

variable "private_key" {
  default = "~/.ssh/tf-keypair.pem"
}

variable "ansible_path" {
  default = "../ansible"
}

variable "memcached_port" {
  default = 11211
}

variable "memcached_nodes" {
  default = 1
}

variable "db_user" {
}

variable "db_password" {
}

variable "keypair_name" {
  description = "The aws private key name"
}

variable "elasticsearch_host" {
  default = "loclahost:9200"
}

variable "kibana_host" {
  default = "loclahost:5601"
}

variable "access_key" {
  description = "Aws access key"
}

variable "secret_key" {
  description = "Aws secret key"
}

variable "es_domain" {
  description = "The domain name of aws elasticsearch"
}

variable "aws_access_key_id" {
  description = "The aws access key id"
}

variable "aws_secret_access_key" {
  description = "The aws secret access key"
}

variable "web_domain" {
  description = "The domain for your web site"
}

