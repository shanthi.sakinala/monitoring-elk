resource "aws_instance" "bastion" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = var.keypair_name
  vpc_security_group_ids = [aws_security_group.bastion_sg.id]
  subnet_id = element(aws_subnet.public_subnets.*.id, 0)
  availability_zone = element(var.availability_zones, 0)
  associate_public_ip_address = true
  provisioner "remote-exec" {
    connection {
      host = self.public_ip
      private_key = file(var.private_key)
      user = var.ansible_user
    }
    inline = ["sudo hostnamectl set-hostname bastion"]
  }
  tags = {
    Name = "terraform-aws-bastion"
  }
}

output "bastion_ip" {
  value = aws_instance.bastion.public_ip
}

