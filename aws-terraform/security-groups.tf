resource "aws_security_group" "web_sg" {
  description = "Allow incoming http connections. "
  name = "terraform-aws-web-sg"
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    security_groups = [aws_security_group.alb_sg.id]
  }
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
    //security_groups = [aws_security_group.bastion_sg.id]
  }
  ingress {
    from_port = 443
    protocol = "tcp"
    to_port = 443
    security_groups = [aws_security_group.alb_sg.id]
  }
  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-aws-web"
  }
}

resource "aws_security_group" "bastion_sg" {
  description = "Allow incoming http connections."
  name = "terraform-aws-bastion-sg"
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-aws-bastion"
  }
}

resource "aws_security_group" "logstash_sg" {
  description = "Allow incoming http connections."
  name = "terraform-aws-logstash-sg"
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
    //security_groups = [aws_security_group.bastion_sg.id]
  }
  ingress {
    from_port = 5044
    protocol = "tcp"
    to_port = 5044
    security_groups = [aws_security_group.web_sg.id]
    //cidr_blocks = [aws_vpc.main_vpc.cidr_block]
  }
  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-aws-logstash"
  }
}

resource "aws_security_group" "es_sg" {
  description = "Allow incoming http connections."
  name = "terraform-aws-es-sg"

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = [aws_vpc.main_vpc.cidr_block]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-aws-es"
  }
}

resource "aws_security_group" "db_sg" {
  description = "Allow incoming database connections from vpc web instances"
  name = "terraform-aws-db-sg"
  ingress {
    from_port = 3306
    protocol = "tcp"
    to_port = 3306
    security_groups = [aws_security_group.web_sg.id]
  }
  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
    cidr_blocks = [aws_vpc.main_vpc.cidr_block]
  }
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-aws-db"
  }
}

resource "aws_security_group" "alb_sg" {
  description = "Allow incoming http connections. "
  name = "terraform-aws-alb-sg"
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    protocol = "tcp"
    to_port = 443
    cidr_blocks = ["0.0.0.0/0"]
  }
 egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-aws-alb"
  }
}

resource "aws_security_group" "memcached_sg" {
  description = "ElastiCache security group"
  vpc_id = aws_vpc.main_vpc.id
  name = "terraform-aws-memcached-sg"
  ingress {
    from_port = 11211
    protocol = "tcp"
    to_port = 11211
    security_groups = [aws_security_group.web_sg.id]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "terraform-aws-memcached"
  }
}