# provision app vpc
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    Name = "terraform-aws"
  }
}

# create public subnets
resource "aws_subnet" "public_subnets" {
  count = length(var.public_subnet_cidrs)
  cidr_block = element(var.public_subnet_cidrs, count.index )
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = element(var.availability_zones, count.index)
  tags = {
    Name = "terraform-aws-public-${count.index + 1}"
  }
}

# create private data subnets
resource "aws_subnet" "private_data_subnets" {
  count = length(var.private_data_subnet_cidrs)
  cidr_block = element(var.private_data_subnet_cidrs, count.index )
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = element(var.availability_zones, count.index)
  tags = {
    Name = "terraform-aws-private-${count.index + 1}"
  }
}

# create igw
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-aws"
  }
}






