resource "aws_route53_zone" "rt53_zone" {
  name = var.web_domain
  tags = {
    Name = "terraform-aws-rt53-zone"
  }
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.rt53_zone.zone_id
  name = "*.${var.web_domain}"
  type = "A"

  alias {
    name = aws_alb.alb.dns_name
    zone_id = aws_alb.alb.zone_id
    evaluate_target_health = true
  }
  depends_on = [aws_alb.alb]
}

output "route_53_name_servers" {
  value = aws_route53_zone.rt53_zone.name_servers
}
