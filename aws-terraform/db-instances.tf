resource "aws_db_instance" "mysql" {
  instance_class = "db.t2.micro"
  allocated_storage = 10
  availability_zone = var.availability_zones[0]
  engine = "mysql"
  engine_version = "5.7"
  storage_type = "gp2"
  name = var.db_name
  username = var.db_user
  password = var.db_password
  parameter_group_name = "default.mysql5.7"
  vpc_security_group_ids = [aws_security_group.db_sg.id]
  db_subnet_group_name = aws_db_subnet_group.mysql.name
  skip_final_snapshot = true
  tags = {
    Name = "terraform-aws-mysql"
  }
}

# create data base subnet group with private data subnets
resource "aws_db_subnet_group" "mysql" {
  name = "db-subnet-group"
  subnet_ids = aws_subnet.private_data_subnets.*.id
  tags = {
    Name = "terraform-aws-db-subnet-group"
  }
}

output "db_endpoint" {
  value = aws_db_instance.mysql.endpoint
}
