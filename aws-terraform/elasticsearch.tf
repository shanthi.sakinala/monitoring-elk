
data "aws_caller_identity" "current" {
}

resource "aws_elasticsearch_domain" "es" {
  domain_name = var.es_domain
  elasticsearch_version = "7.9"

  cluster_config {
    instance_count = 2
    instance_type = "t3.small.elasticsearch"
    zone_awareness_enabled = true
    zone_awareness_config {
      availability_zone_count = 2
    }
  }

  vpc_options {
    subnet_ids = aws_subnet.public_subnets.*.id

    security_group_ids = [aws_security_group.es_sg.id]
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

  access_policies = <<CONFIG
                      {
                        "Version": "2012-10-17",
                        "Statement": [
                          {
                              "Action": "es:*",
                              "Principal": "*",
                              "Effect": "Allow",
                              "Resource": "arn:aws:es:${var.aws_region}:${data.aws_caller_identity.current.account_id}:domain/${var.es_domain}/*"
                          }
                        ]
                      }
                    CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags = {
    Domain = var.es_domain
  }
}

output "es_endpoint" {
  value = aws_elasticsearch_domain.es.endpoint
}

output "kibana_endpoint" {
  value = aws_elasticsearch_domain.es.kibana_endpoint
}