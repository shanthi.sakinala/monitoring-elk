resource "aws_elasticache_cluster" "memcached_cluster" {
  cluster_id = "terraform-aws-cluster"
  engine = "memcached"
  engine_version = "1.5.16"
  maintenance_window   = "sun:05:00-sun:06:00"
  node_type = "cache.t2.micro"
  num_cache_nodes = length(var.private_data_subnet_cidrs)
  port = var.memcached_port
  subnet_group_name = aws_elasticache_subnet_group.memcached.name
  security_group_ids = [aws_security_group.memcached_sg.id]
  az_mode = "cross-az"
  tags = {
    Name = "terraform-aws-memcached"
  }
}

# create elasticache subnet group on the private data subnets
resource "aws_elasticache_subnet_group" "memcached" {
  description = "ElastiCache subnet group"
  name = "memcached-subnet-group"
  subnet_ids = aws_subnet.private_data_subnets.*.id
}

output "memcached_endpoint_address" {
  value = aws_elasticache_cluster.memcached_cluster.configuration_endpoint
}