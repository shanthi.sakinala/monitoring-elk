resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "terraform-aws-public"
  }
}

resource "aws_route_table_association" "public_rt_ass" {
  count = length(var.public_subnet_cidrs)
  route_table_id = aws_route_table.public_rt.id
  subnet_id = element(aws_subnet.public_subnets.*.id, count.index)
}