# Aperçu

Dans ce projet nous allons mettre en place une solution de monitoring basée sur la stack ELK (Elasticsearch-Logstash-Kibana).
Nous aurons l'occasion de voir les deux manières suivantes :
- On premise : en installant nous même la solution ELK sur une machine virtuelle.
- Service managé : en utilisant le service elasticsearch d'Amazon Web Service.

Tout ça est bien, mais ça sera encore meilleur si on arrive à automatiser l'infrastructure à l'aide de Terraform et Ansible.

# Solution de monitoring on promise

## Schéma d'architecture

![On promise infrastructure](images/architecture-on-promise.png)

Pour cette partie le but est d'arriver à construire une solution simple dédiée à monitorer une instance de serveur Nginx.  
Pour collecter des données, nous nous servirons des agents Metricbeat et Filebeat. Ces données seront ensuite transférées pour indexation vers Elasticsearch installé sur une VM locale de type Ubuntu.  
Le module Kibana propose une meilleure visualisation des données sous formes des dashbords.

## Automatisation d'installation

Le script Vagrant permet de construire une VM type Ubuntu sur laquelle sera installée la stack EK(Elasticsearch, Kibana) ainsi que le serveur Nginx à monitorer.  
[Vagrantfile](vagrant/Vagrantfile)

```sh
# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  config.vm.box = "debian/buster64"
  config.vm.box_check_update = false
  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    vb.customize ["modifyvm", :id, "--memory", 4096]
    vb.customize ["modifyvm", :id, "--cpus", "2"]
  end 
  config.vm.define "elk" do |elk|
    elk.vm.hostname = "elk"
    elk.vm.network "forwarded_port", guest: 9200, host: 9200
    elk.vm.network "forwarded_port", guest: 5601, host: 5601
    elk.vm.network "forwarded_port", guest: 80, host: 8080
    elk.vm.network "private_network", ip: "192.168.40.10"
    elk.vm.provision :shell do |s|
      s.path = "init-instance.sh"
    end
  end
  config.vm.provision "shell", inline: <<-SHELL
      # Activate ssh connection for vagrant user
      echo "[1]- Activate ssh connection for vagrant user"
      sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
      service ssh restart
  SHELL
end
````
Le script Vagrant fait appel à un script shell pour initialiser la phase d'installation des composants logiciels.
Pour rester conforme à la version proposée par AWS nous installons la version 7.9.1 de la stack EK.  
À l'écriture de cet article la dernière version est 7.10.1.  
[init-instance.sh](vagrant/init-instance.sh)

```shell
echo "[1]- Install elasticsearch and kibana version 7.x"
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update
sudo apt-get install -q -y vim curl apt-transport-https elasticsearch=7.9.1 kibana=7.9.1 &>/dev/null
sudo cp /vagrant/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml
sudo cp /vagrant/kibana.yml /etc/kibana/kibana.yml
sudo systemctl enable elasticsearch.service
sudo systemctl enable kibana.service
sudo systemctl start elasticsearch.service
sudo systemctl start kibana.service
```

Le script shell exécute automatiquement les opérations suivantes :

#### 1- Installer Kibana et Elasticsearch

La version à installer est 7.9.1

```sh
echo "[1]- Install elasticsearch and kibana version 7.x"
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update
sudo apt-get install -q -y vim curl apt-transport-https elasticsearch=7.9.1 kibana=7.9.1 &>/dev/null
```
#### 2- Mettre à jour la configuration de Kibana

La configuration par défaut de Kibana n'autorise pas l'accès hors VM, il faut mettre à jour le paramètre `server.host` de fichier de configuration `/etc/kibana/kibana.yml`
```yaml
  server.host: "0.0.0.0"
```
#### 3- Installer Filebeat et Metricbeat

Pour collecter les données, le plus simple est d'utiliser les agents Filebeat et Metricbeat qui font partie de la suite ELastic.  
Il faut simplement déployer l'agent sur la machine hôte, associe-le à Elasticsearch et le tour est joué.  
Des configurations par défaut ou nécessitant un minimum de configuration sont proposées pour différents modules tel que (Nginx, system, mongodb ...).

Dans notre cas nous souhaitons monitorer le serveur Nginx, de coup nous aurons besoin de Metricbeat pour collecter les données en rapport avec les connexions et le nombre total de requêtes client, et Filebeat pour collecter les données concernant les logs d’accès et les logs d’erreur.
```sh
#echo "[2]- Install metricbeat version 7.9.1"
curl -L -O -s https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.9.1-amd64.deb
sudo dpkg -i metricbeat-7.9.1-amd64.deb
#echo "[3]- Install filebeat version 7.9.1"
curl -L -O -s https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.9.1-amd64.deb
sudo dpkg -i filebeat-7.9.1-amd64.deb
```

#### 4- Activer le module nginx filebeat
```shell
sudo filebeat modules enable nginx
```
Filebeat a besoin de savoir où se trouve les fichiers de logs à traiter, cette information est à configurer au niveau du fichier `/etc/filebeat/modules.d/nginx.yml`

```yaml
# Module: nginx
# Docs: https://www.elastic.co/guide/en/beats/filebeat/7.10/filebeat-module-nginx.html

- module: nginx
  # Access logs
  access:
    enabled: true
    var.paths: ["/usr/local/nginx/logs/access.log*"]
  # Error logs
  error:
    enabled: true
    var.paths: ["/usr/local/nginx/logs/error.log*"]
```

#### 5- Activer le module nginx metricbeat
```sh
sudo metricbeat modules enable nginx
```
Il faut aussi mettre à jour le fichier de configuration `/etc/metricbeat/modules.d/nginx.yml`
```yaml
# Module: nginx
# Docs: https://www.elastic.co/guide/en/beats/metricbeat/7.9/metricbeat-module-nginx.html
- module: nginx
  metricsets:
    - stubstatus
  period: 10s
  # Nginx hosts
  hosts: ["http://127.0.0.1"]
  # Path to server status. Default server-status
  server_status_path: "server-status"
```
Chaque de 10 secondes, Metricbeat récupère l'état du serveur Nginx local `127.0.0.1` via l'url `/server-status`.

#### 6- Changer la configuration par défaut du module system metricbeat

Le module système est activé par défaut, nous allons juste apporter quelques modifications à son fichier de configuration par défaut.  
Le fichier se trouve au `/etc/metricbeat/modules/system.yml`
   
```yaml
# Module: system
# Docs: https://www.elastic.co/guide/en/beats/metricbeat/7.10/metricbeat-module-system.html

- module: system
  period: 10s
  metricsets:
    - cpu
    - load
    - memory
    - network
    - process
    - diskio
    - service
  process.include_top_n:
    by_cpu: 5      # include top 5 processes by CPU
    by_memory: 5   # include top 5 processes by memory

- module: system
  period: 1m
  metricsets:
    - filesystem
    - fsstat
  processors:
    - drop_event.when.regexp:
        system.filesystem.mount_point: '^/(sys|cgroup|proc|dev|etc|host|lib|snap)($|/)'
```
Chaque 10 secondes, des données systèmes de la virtuelle machine (CPU, MEMORY, DISK ...) seront collectées par l'agent Metricbeat.

#### 7- Lancer le setup de Filebeat

```shell
sudo filebeat setup --index-management --dashboards -E setup.dashboards.directory=/vagrant/conf/filebeat/ &>/dev/null
sudo systemctl start filebeat
```
Cette commande installe automatiquement : 
- Les dashboards kibana se trouvant dans le dossier `/vagrant/conf/filebeat/`
- Charge le modèle (template) de l'index filebeat qui sera utilisé pour indexation des données coté Elasticsearch.


Pour exporter un dashboard sous format json, vous pouvez utiliser la commande suivante :
```shell
sudo metricbeat export dashboard -id [id-dashboard] >> [dashboard-name.json]
```
`id-dashboard:` est récupérable sur l'url de dashboard affiché par Kibana.

#### 8- Lancer le setup de Metricbeat

```shell
sudo metricbeat setup --index-management --dashboards -E setup.dashboards.directory=/vagrant/conf/metricbeat/ &>/dev/null
sudo systemctl start metricbeat
```

Comme pour Filebeat, cette commande installe automatiquement :
- Les dashboards kibana placés dans le dossier `/vagrant/conf/metricbeat/`
- Charge le modèle (template) de l'index metricbeat qui sera utilisé pour indexation des données coté Elasticsearch.

#### 9- Installer le serveur Nginx
```shell
#echo "[4]- Install nginx server version 1.19.6"
sudo apt-get install -q -y libpcre3-dev zlib1g-dev >/dev/null
wget http://nginx.org/download/nginx-1.19.6.tar.gz -q >/dev/null
tar xfz nginx-1.19.6.tar.gz
cd nginx-1.19.6/
./configure --with-http_stub_status_module --with-http_realip_module >/dev/null
make >/dev/null
sudo make install >/dev/null
cp /vagrant/conf/nginx/nginx.conf /usr/local/nginx/conf/nginx.conf
cd /usr/local/nginx/sbin/
sudo ./nginx -c /usr/local/nginx/conf/nginx.conf
sudo ./nginx -s reload
```
Le serveur Nginx sera installé en activant les modules `ngx_http_stub_status_module` et `ngx_http_realip_module`

Comme on a vu précédemment, Metricbeat a besoin de connaître à chaque fois l'état de serveur en passant par l'url `/server-status`.  
Par défaut cette url n'est pas accessible, il faut l'ajouter au niveau de la configuration de serveur.
```editorconfig
location /server-status {
    stub_status on;
    access_log off;
    allow 127.0.0.1;
    deny all;
}
```
Le module `ngx_http_stub_status_module` préalablement installé permet de prendre en charge cette configuration en mettant en place une page web simple avec des données de base représentant l'état de serveur.  
La réponse ressemble à ceci :
```text
Active connections: 291
server accepts handled requests
16630948 16630948 31070465
Reading: 6 Writing: 179 Waiting: 106
```

Le serveur Nginx est installé dans une VM, et pour y accéder depuis l'extérieur nous serons obligé de passer par un `reverse proxy`.  
On va se servir de Nginx comme proxy inverse à installer sur votre machine hôte.
```shell
sudo apt-get install nginx -y
```
Ajoutez le fichier de configuration proxy suivant, et mettez-le dans le dossier `/etc/nginx/sites-available`.
```editorconfig
server {
    listen 80;
    location / {
       proxy_pass http://127.0.0.1:8080;
    }
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
}
```
Toutes les connections arrivant sur le port `80` seront transférées vers le serveur Nginx installé sur la VM et accessible sur le port `8080`.

Le module `ngx_http_realip_module` permet de récupérer l'adresse IP réelle du client, celui qui a initialisé la requête la première fois.  
Vous aurez besoin d'ajouter ces deux lignes à la configuration de serveur Nginx installé sur la VM.
```editorconfig
set_real_ip_from 0.0.0.0/0;
real_ip_header X-Real-IP;
```

Si vous êtes comme moi derrière une box, n'oubliez pas d'ouvrir le port http `80`.

## Accès aux dashborads Kibana

Une fois l'installation terminée, Kibana est désormais accessible via le lien suivant :  
[http://localhost:5601/](http://localhost:5601/)
  
Pour voir les données analysées, il faut choisir l'index pattern correspondant. 

Deux index patterns ont été ajoutés :

- `filebeat-*` index pattern

![Filebeat index](images/filebeat-index.png)

- `metricbeat-*`  index pattern

![Metriceat index](images/metricbeat-index.png)

Vous pouvez aussi consulter les dashboards kibana que nous avons importés via les agents Filebeat et Metricbeat.

- Nginx filebeat dashboard

![Nginx filebeat overview](images/nginx-filebeat-dashboard.png)

- Nginx metricbeat dashboard
  
![Nginx metricbeat overview](images/nginx-metricbeat-dashboard.png)

- Hôte system dashboard

![host-overview](images/host-overview-dashboard.png)

# Solution de monitoring on cloud

## Schéma d'architecture

![aws-wordpress-architecture](images/architecture-on-cloud.png)

Basée sur le service elasticsearch proposé par AWS, l'idée est de mettre en place une solution de monitoring de l'infrastructure déjà réalisée sur un des précédents projets ([aws-terraform-ansible](https://gitlab.com/rsihammou/aws-terraform-ansible)).  
Dans cet article, on ne traitera pas la cloudification de l'infrastructure, si vous avez besoin de voir cet aspect en detail, je vous invite à voir la documentation du projet

Comme présenté sur le schéma, nous disposons de deux instances wordpress à monitorer ainsi que les composants techniques constituant l'infrastructure tel que (MySql RDS, Elasicache ...).
Nous utiliserons aussi les agents Metricbeat et Filebeat pour collecter les données (logs et métriques), ces dernières seront envoyées au serveur logstash installé sur une VM EC2.  
L'output logstash est configuré pour envoyer les données traitées au service aws elasticsearch.

![ELK process](images/elk-process.png)

Les dashboards sont accessible via l'endpoint kibana de l'instance aws elasticsearch, il faut faire attention l'endpoint n'est pas accessible en dehors de VPC.

## Automatisation d'installation

L'infrastructure ainsi que l'installation des briques logicielles sont automatisés via Terraform et Ansible.

```sh
ssh-add -k [aws-ssh-key-path]
terrafom apply -auto-approve
```
### Installation des instances wordpress

L'installation est assurée par le playbook Ansible [playbook-wordpress.yml](ansible/playbook-wordpress.yml), ce dernier est composé des taches suivantes :

#### 1- Installer et configurer le serveur wordpress
Cette étape est bien détaillée sur le précédent projet [aws-terraform-ansible](https://gitlab.com/rsihammou/aws-terraform-ansible).

#### 2- Installer et configurer les agents Filebeat et MetricBeat

Le module AWS de l'agent Metricbeat est sous la licence `xpack`, en plus les services `geoip` et `userAgent` ne sont pas disponible sur AWS elasticsearch.
Pour faire face à ces contraintes nous utiliserons `Logstsh` comme intermédiaire avant d'envoyer les données en indexation.  
Ceci nous permettra aussi de combler les données manquantes de Localisations et d'userAgent.

##### Fichier de configuration des agents beat

[beat-conf.yml.j2](ansible/roles/wordpress/templates/beat-conf.yml.j2)

```yaml
{{ beat }}.config.modules:
  # Glob pattern for configuration loading
  path: ${path.config}/modules.d/*.yml

  # Set to true to enable config reloading
  reload.enabled: false

  # Period on which files under path should be checked for changes
  #reload.period: 10s

# ------------------------------ Logstash Output -------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["{{ logstash_host }}"]

# ================================= Processors =================================
# Configure processors to enhance or manipulate events generated by the beat.
          
processors:
  - add_host_metadata: ~
  - add_cloud_metadata: ~

{% if beat == 'filebeat' %}
processors:
  - drop_event:
      when:
        contains:
          message: "ELB-HealthChecker"
{% endif %}
```
Le fichier template ci-dessus est utilisé pour générer la configuration de Filebeat ou Metricbeat.
Le paramètre beat prend les valeurs `filebeat` et `metricbeat`.  
Les agents beats transfèrent les données vers le serveur Logstash défini par la variable `logstash_host`

Tous les messages en relation avec `ELB-HealthChecker` seront ignorés.

##### Installation des agents beat

```yaml
- name: Install metricbeat
  apt:
    deb: "{{ metricbeat_url }}"

- name: Setup metricbeat
  shell: |
  metricbeat setup --index-management --dashboards \
  -E output.logstash.enabled=false \
  -E setup.ilm.enabled=false \
  -E setup.dashboards.directory=/tmp/metricbeat/ \
  -E output.elasticsearch.hosts=['{{ elasticsearch_host }}'] \
  -E setup.kibana.host={{ kibana_host }}
  run_once: true
```
`metricbeat_url= https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.9.1-amd64.deb`  
Nous installerons la version 7.9.1 de Metricbeat pour rester compatible avec la version actuelle de service aws elasticsearch.

La tache `Setup metricbeat` permet de déployer l'index template et les dashborads se trouvant dans le dossier `/tmp/metricbeat/`.  
Pour le faire nous aurons besoin de :
- Désactiver temporairement la sortie Logstash
- Désactiver le service ILM (index lifecycle management) responsable de la gestion des indices par age puisqu'il est géré aussi par la licence `xpack`.
- Activer temporairement la sortie elasticsearch vers l'endpoint du service aws elasticsearch.
- Activer temporairement l'adresse de kibana serveur en pointant vers l'endpoint kibana du service aws elasticsearch.

Cette tache est exécutée une seule fois `run_once: true`.

```yaml
- name: Install filebeat
  apt:
    deb: "{{ filebeat_url }}"
    
- name: Setup filebeat
  shell: |
  filebeat setup --dashboards \
  -E output.logstash.enabled=false \
  -E setup.ilm.enabled=false \
  -E setup.dashboards.directory=/tmp/filebeat/ \
  -E output.elasticsearch.hosts=['{{ elasticsearch_host }}'] \
  -E setup.kibana.host={{ kibana_host }}
  run_once: true
```
`filebeat_url: https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.9.1-amd64.deb`  
Pour Filebeat, la tache `Setup filebeat` permet simplement de déployer les dashborads se trouvant dans le dossier `/tmp/filebeat/`,
la création de l'index et de son template seront initiés par Logstash.

#### 3- Activer les modules nginx et aws

Pour Filebeat on active le module nginx pour traiter les données de logs des instances wordpress.
```yaml
- name: Enable filebeat Nginx module
  shell: filebeat modules enable nginx
```

Le module AWS permet de récupérer les différentes métriques depuis AWS cloudWatch en utilisant l'API [GetMetricData](https://docs.aws.amazon.com/AmazonCloudWatch/latest/APIReference/API_GetMetricData.html).  
Le module sera activé sur une seule instance wordpress.
```yaml
- name: Enable metricbeat AWS  module
  shell: metricbeat modules enable aws
  run_once: true
```
[aws.yml.j2](ansible/roles/wordpress/templates/aws.yml.j2)
```yaml
# Module: aws
# Docs: https://www.elastic.co/guide/en/beats/metricbeat/7.9/metricbeat-module-aws.html
- module: aws
  period: 5m
  metricsets:
    - ec2
    - ebs
    - rds
    - elb
      access_key_id: '{{ access_key }}'
      secret_access_key: '{{ secret_key }}'
      default_region: '{{ region }}'

- module: aws
  period: 5m
  metricsets:
    - cloudwatch
      metrics:
    - namespace: AWS/ElastiCache
      name: CPUUtilization
      statistic: ["Average"]
    - namespace: AWS/ElastiCache
      name: CurrConfig
      statistic: ["Sum"]
    - namespace: AWS/ElastiCache
      name: Evictions
      statistic: ["Average"]
    - namespace: AWS/ElastiCache
      name: FreeableMemory
      statistic: ["Average"]
    - namespace: AWS/ElastiCache
      name: UnusedMemory
      statistic: ["Average"]
    - namespace: AWS/ElastiCache
      name: SwapUsage
      statistic: ["Average"]

  access_key_id: '{{ access_key }}'
  secret_access_key: '{{ secret_key }}'
  default_region: '{{ region }}'
```
Par défaut, l'intervalle de récupération des métriques est de 5 minutes. Bien sur vous pouvez réduire cet interval pour plus de précisions mais cela engendrera des couts supplémentaires.  
Pour notre exemple, nous aurons besoins de récupérer les métriques des services suivants :
- ec2
- ebs
- rds
- elb
- elastiCache

Le module AWS nécessite les données d'authentification de votre compte AWS (`access_key_id`, `secret_access_key` et `default_region`) afin d'effectuer des appels vers l'API AWS.

### Installation de Logstash

Le playbook Ansible [playbook-logstash.yml](ansible/playbook-logstash.yml) regroupe les différentes taches d'installation suivantes :

#### 1- Installer Logstash
D'abord, il faut installer les prérequis tel que jdk 1.8 ou plus. Ensuite ajouter la clef et le repository Elastic.

```yaml
- name: Install prerequisite for logstash
  apt:
    name: "{{ prerequisite_packages }}"
    state: latest
    update_cache: yes
  register: installed_packages
  until: installed_packages is succeeded

- name: Add elastic repositorie keys
  apt_key:
    url: "{{ elastic_key }}"
    state: present

- name: Add elastic repository
  apt_repository:
    repo: "{{ elastic_repo }}"
    state: present

- name: Install logstash
  apt:
    name: ['logstash']
    state: present
    update_cache: yes
  register: installed_packages
  until: installed_packages is succeeded
```
```yaml
elastic_key: https://artifacts.elastic.co/GPG-KEY-elasticsearch
elastic_repo: "deb https://artifacts.elastic.co/packages/7.x/apt stable main"
prerequisite_packages:
  - apt-transport-https
  - curl
  - vim
  - default-jdk
```

#### 2- Déployer le template de Filebeat index

Comme vu précédemment, la gestion de l'index Filebeat est confiée à Logstash. Bien évidement nous aurons besoin d'une structure de données ou mapping pour analyser les données reçus depuis Filebeat.  
Je me suis servi de la template par défaut que j'ai modelé pour répondre à mon besoin (Modification de mapping pour la GeoIp, userAgent...).

Vous pouvez récupérer la template par défaut comme suivant :
```shell
sudo filebeat export template > filebeat.template.json
```
```yaml
- name: copy filebeat template
  copy:
    src: files/filebeat.template.json
    dest: /usr/share/filebeat/filebeat.template.json
    owner: ubuntu
    group: ubuntu
    mode: 0644
```
#### 3- Installer GeoIp database

Logstash a besoin d'une base de données pour transformer les IPs en localisation, nous utiliserons la base gratuite [GeoLite2-City](https://dev.maxmind.com/geoip/geoip2/geolite2/).
```yaml
- name: copy GeoIp database
  copy:
    src: files/GeoLite2-City.mmdb
    dest: /usr/share/filebeat/GeoLite2-City.mmdb
    owner: ubuntu
    group: ubuntu
    mode: 0655
```
#### 4- Installer amazon-es plugin

Pour communiquer Logstash avec AWS elasticsearch, nous aurons besoin d'installer le plugin `amazon-es`

```yaml
- name: Install amazon-es plugin
  shell: |
    ./logstash-plugin update
    ./logstash-plugin install logstash-output-amazon_es
  args:
    chdir: /usr/share/logstash/bin/
```

#### 5- Ajouter Logstash configuration

```yaml
- name: Copy logstash configuration
  template:
    src: logstash.yml.j2
    dest: /etc/logstash/conf.d/aws-es-output.conf
```
[logstash.yml.j2](ansible/roles/logstash/templates/logstash.yml.j2)

```toml
input {
  beats {
    port => 5044
  }
}
filter {
  if([agent][type] == "filebeat" and [service][type] == "nginx"){
    if [fileset][name] == "access" {
      grok {
        match => { "message" => ["%{IPORHOST:[source][ip]} - %{DATA:[user][name]} \[%{HTTPDATE:[nginx][access][time]}\] \"%{WORD:[http][request][method]} %{DATA:[url][original]} HTTP/%{NUMBER:[http][version]}\" %{NUMBER:[http][response][status_code]} %{NUMBER:[http][response][body][bytes]} \"%{DATA:[http][request][referrer]}\" \"%{DATA:[user_agent][original]}\""] }
        remove_field => "message"
      }
      if([url][original] =~ /(.*\.(js|css|svg|png|gif|jpg|jpeg|tif|json|txt|ico))|(\/server-status)/){
        drop{ }
      }
      mutate {
        rename => { "@timestamp" => "[event][created]" }
      }
      date {
        match => [ "[nginx][access][time]", "dd/MMM/YYYY:H:m:s Z" ]
        remove_field => "[nginx][access][time]"
      }
      useragent {
        source => "[user_agent][original]"
        target => "user_agent"
      }
      mutate {
        add_field => { "[event][category]" => "web" }
      }
      mutate {
        add_field => { "[event][type]" => "access" }
      }
      geoip {
        source => "[source][ip]"
        target => "[source][geo]"
        database => "/usr/share/filebeat/GeoLite2-City.mmdb"
      }
    } else if [fileset][name] == "error" {
      grok {
        match => { "message" => ["%{DATA:[nginx][error][time]} \[%{DATA:[log][level]}\] %{NUMBER:[process][pid]}#%{NUMBER:[process][thread][id]}: (\*%{NUMBER:[nginx][error][connection_id]} )?%{GREEDYDATA:[nginx][error][message]}"] }
      }
      mutate {
        rename => { "[nginx][error][message]" => "message" }
      }
      mutate {
        rename => { "@timestamp" => "[event][created]" }
      }
      date {
        match => [ "[nginx][error][time]", "YYYY/MM/dd H:m:s" ]
        remove_field => "[nginx][error][time]"
      }
      mutate {
        add_field => { "[event][category]" => "web" }
      }
      mutate {
        add_field => { "[event][type]" => "error" }
      }
    }
  }
}
output {
  if([agent][type] == "filebeat") {
    amazon_es {
      hosts => ["{{ elasticsearch_host }}"]
      region => "{{ region }}"
      aws_access_key_id => '{{ access_key }}'
      aws_secret_access_key => '{{ secret_key }}'
      index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+yyyy.MM.dd}"
      manage_template => true
      template => "/usr/share/filebeat/filebeat.template.json"
      template_name => "filebeat-7.9.1"
      template_overwrite => true
    }
  } else if([agent][type] == "metricbeat") {
    amazon_es {
      hosts => ["{{ elasticsearch_host }}"]
      region => "{{ region }}"
      aws_access_key_id => '{{ access_key }}'
      aws_secret_access_key => '{{ secret_key }}'
      index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+yyyy.MM.dd}"
    }
  }
}
```

Logstash écoute sur le port `5044`, c'est le même port utilisé pour les Filebeat et Metricbeat pour envoyer leurs données.  
Si les données sont issues de l'agent Filebeat et concerne le service nginx, alors elles seront parsées suivant le pattern défini au niveau de `grok` bloc. Des données supplémentaires (GeoIp, userAgent) y seront ajoutées.

À la première communication avec AWS elasticsearch :
- La template défini dans le fichier `/usr/share/filebeat/filebeat.template.json` sera déployée sous l'identifiant `filebeat-7.9.1`
- Un index Filebeat de la forme `filebeat-7.9.1-yyy.MM.dd` sera automatiquement crée. 

Dans le cas où les données concerne l'agent Metricbeat elles seront directement transférer pour indexation vers AWS elasticsearch.  
À la première communication avec AWS elasticsearch, un index Metricbeat de la forme `metricbeat-7.9.1-yyy.MM.dd` sera automatiquement crée.

## Accès aux dashborads Kibana

La solution la plus simple pour accéder au service AWS elasticsearch est de passer par un tunnel SSH.  
Ajoutez le fichier `auto-generated/config` généré automatiquement dans votre dossier `~/.ssh/`, ensuite initialiser le tunnel SSH de la manière suivante :
```shell
ssh estunnel -N  -i [aws-ssh-key-path]
```
L'interface kibana est désormais accessible en passant par l'url : [https://localhost:9300/_plugin/kibana/](https://localhost:9300/_plugin/kibana/)

- Nginx logs dashboard

![Nginx filebeat overview](images/nginx-logs-overview-aws-dashboard.png)

- AWS metricbeat dashboard

![Nginx metricbeat overview](images/aws-metricbeat-overview-dashboard-1.png)
![Nginx metricbeat overview](images/aws-metricbeat-overview-dashboard-2.png)

# Kibana alerting

Le service aws elasticsearch propose un system d'alerting intuitif et simple d'utilisation composé de trois parties :

#### 1- Destinations
Actuellement, vous pouvez choisir parmi les quatre types de notifications suivants : 
- Slack
- Amazon SNS
- Amazon chime
- Custom webhook

Pour notre projet, nous allons choisir les notifications via Slack.

Rendez-vous sur la page :  `Alerting > distinations > Add distination`
![Aouter une destination](images/add-destination.png)

Ajoutez votre Slack webhook url.

#### 2- Moniteurs

Sélectionnez `Alerting > Monitors > Create monitor`
![Créer un moniteur](images/create-monitor.png)
Choisissez un nom pour le moniteur.

Pour définir le moniteur, vous avez trois possibilités : 
- Définition visuelle, plus adaptée pour les moniteurs de type "une valeur est supérieure ou inférieure à un certain seuil pendant un certain temps".
- Définition via des requêtes Elasticsearch query DSL, cette méthode vous offre plus de flexibilité.
- On se basant sur le détecteur d'anomalie qui doit être créer au préalable.

La première solution est la plus adapté à notre situation, vu qu'on cherche à créer une alerte en cas de saturation de disque (une limite à ne pas dépasser).
- Choisissez l'option `Define using visual graph`
- Choisissez l'index `metricbeat-7.9.1-yyyyMMdd`
- Choisissez l'agrégation count (), l'ensemble de documents et la période de dernière heure.
- L'alerte est prise en compte quand le paramètre `system.fsstat.total_size.free` est inférieur à `10GB`

#### 3- Déclencheur

Une fois le moniteur est créé, vous entamé l'étape de création de déclencheur. Vous commencez par spécifier un nom et un niveau de gravité pour le déclencheur. Les niveaux de gravité vous aident à gérer les alertes.  
![Créer un trigger](images/create-trigger.png)
Choisissez la condition `IS ABOVE 1` pour la condition de trigger, l'alerte sera déclenchée une fois le seuil est affranchi.  
![Configuration de l'action](images/configure-action.png)
Pour La partie action, choisissez la notification Slack créée précédemment, vous avez la possibilité de spécifier le message de la notification suivant vos besoins.

Si l'alerte est déclenchée, vous allez recevoir une notification Slack comme l'exemple suivant : 

![Notification Slack](images/slack-notifications.png)